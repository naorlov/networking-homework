import typing as t
from dslib import Context, Message, Node


class EndNode(Node):
    # DO NOT CHANGE THIS CLASS
    def __init__(self, node_ip, router_ip):
        self._ip = node_ip
        self._router_ip = router_ip

    def on_message(self, msg: Message, sender: str, ctx: Context):
        print(f"[EndNode] Got message from {sender}, sending to local")
        if msg["dst_addr"] == self._ip:
            ctx.send_local(msg)

    def on_local_message(self, msg: Message, ctx: Context):
        print(f"[EndNode] Got local message {msg}, sending to my router")
        ctx.send(msg, self._router_ip)


class Router(Node):
    def __init__(
            self,
            node_ip: str,
            node_mask: str,
            neighbors: t.Tuple[t.Tuple[str, str]],
            routing_table: t.Tuple[t.Tuple[str, str, str]],
    ):
        self._ip = node_ip
        self._node_mask = node_mask

        # Ip addresses of connected routers.
        # Feel free to change it to suit your needs
        self._neighbors = neighbors or []

        # On initialization consists of tuples (network_ip, network_mask, next_hop).
        # Feel free to change it to suit your needs
        self._routing_table = routing_table or []
        # Feel free to add your initialization code here

    def on_message(self, msg: Message, sender: str, ctx: Context):
        # TODO: Process messages from router and nodes
        pass

    def on_timer(self, timer_id: str, ctx: Context):
        pass
