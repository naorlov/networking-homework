use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::collections::HashSet;
use std::hash::{Hash, Hasher};

use dslib::node::LocalEventType;
use dslib::pynode::JsonMessage;
use dslib::system::{SysEvent, System};
use dslib::test::TestResult;

use crate::{Payload, TestConfig, topologies};

fn hash(string: &str) -> u64 {
    let mut hasher = DefaultHasher::new();
    string.hash(&mut hasher);
    hasher.finish()
}

struct MessageGenerator {
    message_id: u32,
}

impl MessageGenerator {
    fn generate(&mut self, from: &str, to: &str) -> JsonMessage {
        let message_payloads = [
            "networks are amazing",
            "here we are",
            "Valtteri, it's James",
            "Sir Lancelot",
            "blow your mind",
            "let's do a barell roll",
        ];
        let idx = (hash(from) + hash(to)) as usize % message_payloads.len();
        let message = message_payloads[idx].to_string();

        self.message_id += 1;

        JsonMessage::from(
            "plain",
            &Payload {
                src_addr: from,
                dst_addr: to,
                msg_id: self.message_id,
                payload: &message,
            },
        )
    }
}

fn extract_all_messages(sys: &System<JsonMessage>) -> Vec<JsonMessage> {
    let mut all_recieved_messages = Vec::<JsonMessage>::new();

    for nodeid in sys.get_node_ids() {
        for event in sys.get_local_events(&nodeid) {
            if matches!(event.tip, LocalEventType::LocalMessageSend) {
                all_recieved_messages.push(event.msg.unwrap().clone());
            }
        }
    }

    all_recieved_messages
}

pub(crate) fn check_delivery(sys: &System<JsonMessage>, messages: &[JsonMessage]) -> TestResult {
    let all_recieved_messages = extract_all_messages(sys);

    let sent_messages_by_destination = build_messages_by_destination(messages);
    let recieved_messages_by_destination = build_messages_by_destination(&*all_recieved_messages);

    let sent_messages_hosts = sent_messages_by_destination.keys().collect::<HashSet<_>>();
    let recieved_messages_hosts = recieved_messages_by_destination
        .keys()
        .collect::<HashSet<_>>();

    if sent_messages_hosts
        .symmetric_difference(&recieved_messages_hosts)
        .count()
        != 0
    {
        for host in recieved_messages_hosts.difference(&sent_messages_hosts) {
            return Err(format!(
                "Host recieved messages contains messages that were not sent. Check {}",
                host
            ));
        }
    }

    for (dst, messages) in sent_messages_by_destination {
        if !recieved_messages_by_destination.contains_key(&*dst) {
            return Err(format!("No message got to {}", dst));
        }

        for message in messages {
            if !recieved_messages_by_destination
                .get(&*dst)
                .unwrap()
                .contains(&message)
            {
                return Err(format!("Message {} is not delivered", message));
            }
        }
    }

    Ok(true)
}

fn build_messages_by_destination(messages: &[JsonMessage]) -> HashMap<String, HashSet<Payload>> {
    let mut messages_by_destination = HashMap::new();
    for msg in messages {
        let payload = msg.to::<Payload>();
        messages_by_destination
            .entry(payload.dst_addr.parse().unwrap())
            .or_insert(HashSet::new())
            .insert(*payload);
    }
    messages_by_destination
}

fn send_messages(sys: &mut System<JsonMessage>, messages: &[JsonMessage]) {
    for msg in messages {
        sys.send_local(msg.clone(), &msg.to::<Payload>().src_addr);
        sys.step();
    }
}

pub(crate) fn test_message_delivered_on_same_network(config: &TestConfig) -> TestResult {
    let mut sys = topologies::simple(&config);
    let mut gen = MessageGenerator { message_id: 0 };
    let messages = [gen.generate("1.1.1.2", "1.1.1.3")];

    sys.step_while(event_is_initialization_timer);
    println!("--- Initialization complete");

    println!("--- Sending messages");
    send_messages(&mut sys, &messages);

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);
    check_delivery(&sys, &messages)
}

pub(crate) fn test_message_delivered_across_networks(config: &TestConfig) -> TestResult {
    let mut sys = topologies::line(&config);
    let mut gen = MessageGenerator { message_id: 0 };
    let messages = [gen.generate("1.1.1.2", "2.2.2.2")];

    sys.step_while(event_is_initialization_timer);
    println!("--- Initialization complete");

    println!("--- Sending messages");
    send_messages(&mut sys, &messages);

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);
    check_delivery(&sys, &messages)
}

pub(crate) fn test_message_delivered_through_networks(config: &TestConfig) -> TestResult {
    let mut sys = topologies::line3_with_routing_info(&config);
    let mut gen = MessageGenerator { message_id: 0 };
    let messages = [gen.generate("1.1.1.2", "3.3.3.2")];

    sys.step_while(event_is_initialization_timer);
    println!("--- Initialization complete");

    println!("--- Sending messages");
    send_messages(&mut sys, &messages);

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);
    check_delivery(&sys, &messages)
}

pub(crate) fn test_with_empty_routing_information(config: &TestConfig) -> TestResult {
    let mut sys = topologies::ring4(&config);
    let mut gen = MessageGenerator { message_id: 0 };
    let messages = [gen.generate("1.1.1.2", "3.3.3.2")];

    sys.step_while(event_is_initialization_timer);
    println!("--- Initialization complete");

    sys.steps(100);

    println!("--- Sending messages");
    send_messages(&mut sys, &messages);

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);
    check_delivery(&sys, &messages)
}

pub(crate) fn test_with_router_failure(config: &TestConfig) -> TestResult {
    let mut sys = topologies::ring4(&config);
    let mut gen = MessageGenerator { message_id: 0 };
    let mut messages = vec![gen.generate("1.1.1.2", "3.3.3.2")];

    sys.step_while(event_is_initialization_timer);
    println!("--- Initialization complete");

    println!("--- Stepping 100 events");
    sys.steps(100);

    println!("--- Sending messages");
    send_messages(&mut sys, &messages);

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);

    println!("--- Disconnecting 2.2.2.1");
    sys.disconnect_node("2.2.2.1");
    sys.steps(100);
    println!("--- Sending to 1.1.1.2");
    messages.push(gen.generate("1.1.1.2", "3.3.3.2"));
    sys.send_local(messages.last().unwrap().clone(), "1.1.1.2");

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);

    println!("--- Reconnecting node 2.2.2.1");
    sys.connect_node("2.2.2.1");

    println!("--- Disconnecting node 4.4.4.1");
    sys.disconnect_node("4.4.4.1");

    println!("--- Stepping 100 events");
    sys.steps(100);

    println!("--- Sending to 1.1.1.2");
    messages.push(gen.generate("1.1.1.2", "3.3.3.2"));
    sys.send_local(messages.last().unwrap().clone(), "1.1.1.2");

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);

    check_delivery(&sys, &messages)
}

pub(crate) fn test_with_router_failure_different_masks(config: &TestConfig) -> TestResult {
    let mut sys = topologies::ring4_with_different_masks(&config);
    let mut gen = MessageGenerator { message_id: 0 };
    let mut messages = vec![gen.generate("1.1.1.2", "3.3.3.2")];

    sys.step_while(event_is_initialization_timer);
    println!("--- Initialization complete");

    println!("--- Stepping 100 events");
    sys.steps(100);

    println!("--- Sending messages");
    send_messages(&mut sys, &messages);

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);

    println!("--- Disconnecting 2.2.2.1");
    sys.disconnect_node("2.2.2.1");
    sys.steps(100);
    println!("--- Sending to 1.1.1.2");
    messages.push(gen.generate("1.1.1.2", "3.3.3.2"));
    sys.send_local(messages.last().unwrap().clone(), "1.1.1.2");

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);

    println!("--- Reconnecting node 2.2.2.1");
    sys.connect_node("2.2.2.1");

    println!("--- Disconnecting node 4.4.4.1");
    sys.disconnect_node("4.4.4.1");

    println!("--- Stepping 100 events");
    sys.steps(100);

    println!("--- Sending to 1.1.1.2");
    messages.push(gen.generate("1.1.1.2", "3.3.3.2"));
    sys.send_local(messages.last().unwrap().clone(), "1.1.1.2");

    println!("--- Stepping while messages are propagating in system");
    sys.step_while(event_is_not_bgp_related);

    check_delivery(&sys, &messages)
}

fn event_is_initialization_timer(event: &SysEvent<JsonMessage>) -> bool {
    match event {
        SysEvent::TimerFired { name, .. } => name == "init",
        SysEvent::TimerSet { name, .. } => name == "init",
        _ => false,
    }
}

fn event_is_not_bgp_related(event: &SysEvent<JsonMessage>) -> bool {
    match event {
        SysEvent::MessageSend { msg, .. } => {
            return !msg.tip.starts_with("bgp");
        }
        SysEvent::MessageReceive { msg, .. } => {
            return !msg.tip.starts_with("bgp");
        }
        SysEvent::TimerSet { name, .. } => {
            return !name.starts_with("bgp");
        }
        SysEvent::TimerFired { name, .. } => {
            return !name.starts_with("bgp");
        }
        _ => true,
    }
}
