use dslib::pynode::JsonMessage;
use dslib::system::System;

use crate::rc;
use crate::refcell;
use crate::TestConfig;

pub(crate) fn simple(config: &TestConfig) -> System<JsonMessage> {
    /*
                 +--------+
                 | router |
                 +--------+
                   ^    ^
    +--------+     |    |     +--------+
    | node_1 | <---+    +---> | node_2 |
    +--------+                +--------+
     */

    let mut sys = System::with_seed(config.seed);

    let mask_24 = "255.255.255.0";
    let alpha_ip = "1.1.1.1";
    let node_1_ip = "1.1.1.2";
    let node_2_ip = "1.1.1.3";

    let router_alpha = config.router_f.build(alpha_ip, (alpha_ip, mask_24, (), ()));
    let node_1 = config.node_f.build(node_1_ip, (node_1_ip, alpha_ip));
    let node_2 = config.node_f.build(node_2_ip, (node_2_ip, alpha_ip));

    sys.add_node(rc!(refcell!(router_alpha)));
    sys.add_node(rc!(refcell!(node_1)));
    sys.add_node(rc!(refcell!(node_2)));

    sys.disable_all_links();
    sys.enable_between(node_1_ip, alpha_ip);
    sys.enable_between(node_2_ip, alpha_ip);
    sys
}

pub(crate) fn line(config: &TestConfig) -> System<JsonMessage> {
    /*
    +--------------+                                      +--------------+
    |    node_1    | <---+                          +---> |    node_2    |
    +--------------+     |                          |     +--------------+
                         |                          |
                         |                          |
                         |                          |
                         v                          v
                 +--------------+           +-------------+
                 | router-alpha | <-------> | router-beta |
                 +--------------+           +-------------+
    */

    let mut sys = System::with_seed(config.seed);

    let mask_24 = "255.255.255.0";
    let alpha_ip = "1.1.1.1";
    let beta_ip = "2.2.2.1";
    let node_1_ip = "1.1.1.2";
    let node_2_ip = "2.2.2.2";

    let router_alpha = config.router_f.build(
        alpha_ip,
        (
            alpha_ip,
            mask_24,
            ((beta_ip, mask_24),),
            ((beta_ip, mask_24, beta_ip),),
        ),
    );

    let router_beta = config.router_f.build(
        beta_ip,
        (
            beta_ip,
            mask_24,
            ((alpha_ip, mask_24),),
            ((alpha_ip, mask_24, alpha_ip),),
        ),
    );

    let node_1 = config.node_f.build(node_1_ip, (node_1_ip, alpha_ip));
    let node_2 = config.node_f.build(node_2_ip, (node_2_ip, beta_ip));

    sys.add_node(rc!(refcell!(router_alpha)));
    sys.add_node(rc!(refcell!(router_beta)));
    sys.add_node(rc!(refcell!(node_1)));
    sys.add_node(rc!(refcell!(node_2)));

    sys.disable_all_links();
    sys.enable_between(node_1_ip, alpha_ip);
    sys.enable_between(alpha_ip, beta_ip);
    sys.enable_between(beta_ip, node_2_ip);

    return sys;
}

pub(crate) fn line3_with_routing_info(config: &TestConfig) -> System<JsonMessage> {
    let mut sys = System::with_seed(config.seed);

    let mask_24 = "255.255.255.0";
    let alpha_ip = "1.1.1.1";
    let beta_ip = "2.2.2.1";
    let gamma_ip = "3.3.3.1";

    let alpha_node_1_ip = "1.1.1.2";
    let alpha_node_2_ip = "1.1.1.3";

    let beta_node_1_ip = "2.2.2.2";
    let beta_node_2_ip = "2.2.2.3";

    let gamma_node_1_ip = "3.3.3.2";
    let gamma_node_2_ip = "3.3.3.3";

    let router_alpha = config.router_f.build(
        alpha_ip,
        (
            alpha_ip,
            mask_24,
            ((beta_ip, mask_24),),
            ((beta_ip, mask_24, beta_ip), (gamma_ip, mask_24, beta_ip)),
        ),
    );

    let router_beta = config.router_f.build(
        beta_ip,
        (
            beta_ip,
            mask_24,
            ((alpha_ip, mask_24), (gamma_ip, mask_24)),
            ((alpha_ip, mask_24, alpha_ip), (gamma_ip, mask_24, beta_ip)),
        ),
    );

    let router_gamma = config.router_f.build(
        gamma_ip,
        (
            gamma_ip,
            mask_24,
            ((beta_ip, mask_24),),
            ((alpha_ip, mask_24, beta_ip), (beta_ip, mask_24, beta_ip)),
        ),
    );

    let alpha_node_1 = config
        .node_f
        .build(alpha_node_1_ip, (alpha_node_1_ip, alpha_ip));
    let alpha_node_2 = config
        .node_f
        .build(alpha_node_2_ip, (alpha_node_2_ip, alpha_ip));
    let beta_node_1 = config
        .node_f
        .build(beta_node_1_ip, (beta_node_1_ip, beta_ip));
    let beta_node_2 = config
        .node_f
        .build(beta_node_2_ip, (beta_node_2_ip, beta_ip));
    let gamma_node_1 = config
        .node_f
        .build(gamma_node_1_ip, (gamma_node_1_ip, gamma_ip));
    let gamma_node_2 = config
        .node_f
        .build(gamma_node_2_ip, (gamma_node_2_ip, gamma_ip));

    sys.add_node(rc!(refcell!(router_alpha)));
    sys.add_node(rc!(refcell!(router_beta)));
    sys.add_node(rc!(refcell!(router_gamma)));
    sys.add_node(rc!(refcell!(alpha_node_1)));
    sys.add_node(rc!(refcell!(alpha_node_2)));
    sys.add_node(rc!(refcell!(beta_node_1)));
    sys.add_node(rc!(refcell!(beta_node_2)));
    sys.add_node(rc!(refcell!(gamma_node_1)));
    sys.add_node(rc!(refcell!(gamma_node_2)));
    sys.disable_all_links();

    sys.enable_between(alpha_ip, beta_ip);
    sys.enable_between(beta_ip, gamma_ip);

    sys.enable_between(alpha_node_1_ip, alpha_ip);
    sys.enable_between(alpha_node_2_ip, alpha_ip);
    sys.enable_between(beta_node_1_ip, beta_ip);
    sys.enable_between(beta_node_2_ip, beta_ip);
    sys.enable_between(gamma_node_1_ip, gamma_ip);
    sys.enable_between(gamma_node_2_ip, gamma_ip);

    return sys;
}

pub(crate) fn ring4(config: &TestConfig) -> System<JsonMessage> {
    let mut sys = System::with_seed(config.seed);
    let mask_24 = "255.255.255.0";
    let mask_30 = "255.255.255.252";
    let alpha_ip = "1.1.1.1";
    let beta_ip = "2.2.2.1";
    let gamma_ip = "3.3.3.1";
    let theta_ip = "4.4.4.1";

    let alpha_node_1_ip = "1.1.1.2";
    let alpha_node_2_ip = "1.1.1.3";

    let beta_node_1_ip = "2.2.2.2";
    let beta_node_2_ip = "2.2.2.3";

    let gamma_node_1_ip = "3.3.3.2";
    let gamma_node_2_ip = "3.3.3.3";

    let theta_node_1_ip = "4.4.4.2";
    let theta_node_2_ip = "4.4.4.3";

    let router_alpha = config.router_f.build(
        alpha_ip,
        (
            alpha_ip,
            mask_24,
            ((beta_ip, mask_24), (theta_ip, mask_24)),
            (),
        ),
    );

    let router_beta = config.router_f.build(
        beta_ip,
        (
            beta_ip,
            mask_24,
            ((alpha_ip, mask_24), (gamma_ip, mask_24)),
            (),
        ),
    );

    let router_gamma = config.router_f.build(
        gamma_ip,
        (
            gamma_ip,
            mask_24,
            ((theta_ip, mask_24), (beta_ip, mask_24)),
            (),
        ),
    );

    let router_theta = config.router_f.build(
        theta_ip,
        (
            theta_ip,
            mask_24,
            ((alpha_ip, mask_24), (gamma_ip, mask_24)),
            (),
        ),
    );

    let alpha_node_1 = config
        .node_f
        .build(alpha_node_1_ip, (alpha_node_1_ip, alpha_ip));
    let alpha_node_2 = config
        .node_f
        .build(alpha_node_2_ip, (alpha_node_2_ip, alpha_ip));
    let beta_node_1 = config
        .node_f
        .build(beta_node_1_ip, (beta_node_1_ip, beta_ip));
    let beta_node_2 = config
        .node_f
        .build(beta_node_2_ip, (beta_node_2_ip, beta_ip));
    let gamma_node_1 = config
        .node_f
        .build(gamma_node_1_ip, (gamma_node_1_ip, gamma_ip));
    let gamma_node_2 = config
        .node_f
        .build(gamma_node_2_ip, (gamma_node_2_ip, gamma_ip));

    let theta_node_1 = config
        .node_f
        .build(theta_node_1_ip, (theta_node_1_ip, gamma_ip));
    let theta_node_2 = config
        .node_f
        .build(theta_node_2_ip, (theta_node_2_ip, gamma_ip));

    sys.add_node(rc!(refcell!(router_alpha)));
    sys.add_node(rc!(refcell!(router_beta)));
    sys.add_node(rc!(refcell!(router_gamma)));
    sys.add_node(rc!(refcell!(router_theta)));
    sys.add_node(rc!(refcell!(alpha_node_1)));
    sys.add_node(rc!(refcell!(alpha_node_2)));
    sys.add_node(rc!(refcell!(beta_node_1)));
    sys.add_node(rc!(refcell!(beta_node_2)));
    sys.add_node(rc!(refcell!(gamma_node_1)));
    sys.add_node(rc!(refcell!(gamma_node_2)));
    sys.add_node(rc!(refcell!(theta_node_1)));
    sys.add_node(rc!(refcell!(theta_node_2)));

    sys.disable_all_links();
    sys.enable_between(alpha_ip, beta_ip);
    sys.enable_between(alpha_ip, theta_ip);
    sys.enable_between(beta_ip, gamma_ip);
    sys.enable_between(theta_ip, gamma_ip);

    sys.enable_between(alpha_node_1_ip, alpha_ip);
    sys.enable_between(alpha_node_2_ip, alpha_ip);
    sys.enable_between(beta_node_1_ip, beta_ip);
    sys.enable_between(beta_node_2_ip, beta_ip);
    sys.enable_between(gamma_node_1_ip, gamma_ip);
    sys.enable_between(gamma_node_2_ip, gamma_ip);
    sys.enable_between(theta_node_1_ip, theta_ip);
    sys.enable_between(gamma_node_2_ip, theta_ip);

    sys
}

pub(crate) fn ring4_with_different_masks(config: &TestConfig) -> System<JsonMessage> {
    let mut sys = System::with_seed(config.seed);
    let mask_24 = "255.255.255.0";
    let alpha_ip = "1.1.1.1";
    let beta_ip = "2.2.2.1";

    let mask_28 = "255.255.255.240";
    let gamma_ip = "3.3.3.1";
    let theta_ip = "4.4.4.1";

    let alpha_node_1_ip = "1.1.1.2";
    let alpha_node_2_ip = "1.1.1.3";

    let beta_node_1_ip = "2.2.2.2";
    let beta_node_2_ip = "2.2.2.3";

    let gamma_node_1_ip = "3.3.3.2";
    let gamma_node_2_ip = "3.3.3.3";

    let theta_node_1_ip = "4.4.4.2";
    let theta_node_2_ip = "4.4.4.3";

    let router_alpha = config.router_f.build(
        alpha_ip,
        (
            alpha_ip,
            mask_24,
            ((beta_ip, mask_24), (theta_ip, mask_24)),
            (),
        ),
    );

    let router_beta = config.router_f.build(
        beta_ip,
        (
            beta_ip,
            mask_24,
            ((alpha_ip, mask_24), (gamma_ip, mask_24)),
            (),
        ),
    );

    let router_gamma = config.router_f.build(
        gamma_ip,
        (
            gamma_ip,
            mask_28,
            ((theta_ip, mask_24), (beta_ip, mask_24)),
            (),
        ),
    );

    let router_theta = config.router_f.build(
        theta_ip,
        (
            theta_ip,
            mask_28,
            ((alpha_ip, mask_24), (gamma_ip, mask_24)),
            (),
        ),
    );

    let alpha_node_1 = config
        .node_f
        .build(alpha_node_1_ip, (alpha_node_1_ip, alpha_ip));
    let alpha_node_2 = config
        .node_f
        .build(alpha_node_2_ip, (alpha_node_2_ip, alpha_ip));
    let beta_node_1 = config
        .node_f
        .build(beta_node_1_ip, (beta_node_1_ip, beta_ip));
    let beta_node_2 = config
        .node_f
        .build(beta_node_2_ip, (beta_node_2_ip, beta_ip));
    let gamma_node_1 = config
        .node_f
        .build(gamma_node_1_ip, (gamma_node_1_ip, gamma_ip));
    let gamma_node_2 = config
        .node_f
        .build(gamma_node_2_ip, (gamma_node_2_ip, gamma_ip));

    let theta_node_1 = config
        .node_f
        .build(theta_node_1_ip, (theta_node_1_ip, gamma_ip));
    let theta_node_2 = config
        .node_f
        .build(theta_node_2_ip, (theta_node_2_ip, gamma_ip));

    sys.add_node(rc!(refcell!(router_alpha)));
    sys.add_node(rc!(refcell!(router_beta)));
    sys.add_node(rc!(refcell!(router_gamma)));
    sys.add_node(rc!(refcell!(router_theta)));
    sys.add_node(rc!(refcell!(alpha_node_1)));
    sys.add_node(rc!(refcell!(alpha_node_2)));
    sys.add_node(rc!(refcell!(beta_node_1)));
    sys.add_node(rc!(refcell!(beta_node_2)));
    sys.add_node(rc!(refcell!(gamma_node_1)));
    sys.add_node(rc!(refcell!(gamma_node_2)));
    sys.add_node(rc!(refcell!(theta_node_1)));
    sys.add_node(rc!(refcell!(theta_node_2)));

    sys.disable_all_links();
    sys.enable_between(alpha_ip, beta_ip);
    sys.enable_between(alpha_ip, theta_ip);
    sys.enable_between(beta_ip, gamma_ip);
    sys.enable_between(theta_ip, gamma_ip);

    sys.enable_between(alpha_node_1_ip, alpha_ip);
    sys.enable_between(alpha_node_2_ip, alpha_ip);
    sys.enable_between(beta_node_1_ip, beta_ip);
    sys.enable_between(beta_node_2_ip, beta_ip);
    sys.enable_between(gamma_node_1_ip, gamma_ip);
    sys.enable_between(gamma_node_2_ip, gamma_ip);
    sys.enable_between(theta_node_1_ip, theta_ip);
    sys.enable_between(gamma_node_2_ip, theta_ip);

    sys
}
