# Networking Homework

## Первичные действия

1. Сделать приватный форк, выдать доступы преподавателям:
    ```
    naorlov
    arodionov18
    petuhovskiy
    belyakov_am
    ```

2. Скачать репозиторий вместе с сабмодулями
    ```bash
    $ git clone --recurse-submodules YOUR_REPOSITORY
    ```

3. Добавить оригинальный репозиторий как второй remote, чтобы можно было что-нибудь обновить:
    ```bash
    $ git remote add upstream https://gitlab.com/naorlov/networking-homework.git
    ```

## При новой домашке

1. Синхронизироваться с апстримом
    ```bash
    $ git fetch upstream
    $ git checkout main
    $ git merge upstream/main
    ```
2. Сделать новую ветку для очередного домашнего задания
    ```bash
    $ git checkout -b N-taskname
    ```
3. Выполнить задание согласно инструкции в соответствующей директории
4. Запушить ветку
    ```bash
    $ git push -u origin HEAD
    ```
5. Открыть Merge Request из ветки задания в свою ветку main

6. До дедлайна сдать ссылку на Merge Request в [форму](https://forms.gle/9DYjTGm1vQejELZS7). 

> ⚠️ **Временем сдачи домашнего задания будет считаться время начала работы последнего запуска CI. Отправлять ссылку в форму нужно один раз.**

7. По завершении процесса ревью и оценивания смержить ветку