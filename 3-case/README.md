# Домашнее задание 3

В этом домашнем задании вам нужно будет продемонстрировать интуицию насчет компьютерных сетей и понимание того, как эти самые сети устроены, а именно -- решить маленький кейс, который приключился с автором задания в реальной жизни.



## Кейс

Одним вечером лектор оказался вне дома с ноутбуком, смартфоном и переносным роутером (в тексте "модем"). Ожидая встречи, лектор решил поработать. Для работы необходимо подключиться по VPN до дата-центра компании, в которой он работает. С помощью режима работы смартфона в виде точки доступа, он смог подключиться к сетевым ресурсам компании. Спустя небольшое время телефон стал показывать перерывы в связи (в тот день была плохая погода), поэтому лектор решил попоробовать использовать модем для связи со внешним миром. Единственная проблема -- VPN-подключение через модем работало очень странно. Подключение с VPN-сервером устанавливалось, но все запросы в корпоративные системы отваливались с ошибкой "Connection refused". Лектор начал расследование.

При подключении к VPN устанавливаются _частичные_ маршруты -- то есть только пакеты с некоторыми адресами получателя перенаправлялись на VPN сервер. 

Подключенный через _смартфон_ VPN работал без ошибок:

```
$ ping internal-resource.company.ru
64 bytes from 10.0.0.34: icmp_seq=0 ttl=62 time=99.624 ms
64 bytes from 10.0.0.34: icmp_seq=1 ttl=62 time=96.464 ms
64 bytes from 10.0.0.34: icmp_seq=2 ttl=62 time=94.635 ms
64 bytes from 10.0.0.34: icmp_seq=3 ttl=62 time=92.313 ms
```

```
$ curl internal-resource.company.ru
<html>
<head><title>301 Moved Permanently</title></head>
<body>
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

```
$ dig internal-resource.company.ru
; <<>> DiG 9.10.6 <<>> internal-resource.company.ru
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41973
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;internal-resource.company.ru.		IN	A

;; ANSWER SECTION:
internal-resource.company.ru.	183	IN	A	10.0.0.34
internal-resource.company.ru.	183	IN	A	10.0.0.34
internal-resource.company.ru.	183	IN	A	10.0.0.34

;; Query time: 88 msec
;; SERVER: 10.0.0.1#53(10.0.0.1)
;; WHEN: Thu Dec 10 17:45:50 MSK 2021
;; MSG SIZE  rcvd: 92
```

```
$ ifconfig
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 14:7d:da:12:38:d3
	inet 192.168.1.73 netmask 0xffffff00 broadcast 192.168.1.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
utun0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1500
	inet 10.9.10.11 --> 10.9.10.11 netmask 0xfffff800
	nd6 options=201<PERFORMNUD,DAD>
```

При этом при подключении через *модем* те же команды вернули следующее

```
$ ping internal-resource.company.ru
ping: cannot resolve non-existent-size.com: Unknown host
```

```
$ curl internal-resource.company.ru
curl: (6) Could not resolve host: Unknown host
```

```
$ dig internal-resource.company.ru
; <<>> DiG 9.10.6 <<>> internal-resource.company.ru
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41973
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;internal-resource.company.ru.		IN	A

;; AUTHORITY SECTION:
ru.			5	IN	SOA	a.dns.ripn.net. hostmaster.ripn.net. 4049557 86400 14400 2592000 3600

;; Query time: 88 msec
;; SERVER: 10.0.0.1#53(10.0.0.1)
;; WHEN: Thu Dec 10 17:53:57 MSK 2021
;; MSG SIZE  rcvd: 92
```

```
$ ifconfig
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 14:7d:da:12:38:d3
	inet 10.0.0.5 netmask 0xffffff00 broadcast 10.0.0.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
utun0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1500
	inet 10.9.10.11 --> 10.9.10.11 netmask 0xfffff800
	nd6 options=201<PERFORMNUD,DAD>
```

Вопрос: что нужно изменить в настройках сети, чтобы все вернулось обратно в рабочий режим? Напишите шаги, которые вы бы дополнительно предприняли для устранения проблемы (вариант "написать в поддержку" будет снимать один балл из оценки). При продумывании вариантов учтите, что у лектора был полный доступ до модема и смартфона.

Оценивание будет строиться на разумности рассуждений и задаваемых вопросах.



Дедлайн: воскресенье, 19 декабря, 23:59. Подгрузите markdown-файлик в репозиторий и сдайте домашку по общему формату. 